import io.appium.java_client.MobileDriver;
import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidDriver;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.testng.Assert;
import org.testng.annotations.Test;

import java.net.MalformedURLException;
import java.net.URL;

public class LoginTest {
    public static final String EXPECTED = "Please enter at least 8 characters";
    public static final String EMAIL = "test@test.ru";

    @Test
    public void CheckEmptyPassword() throws MalformedURLException, InterruptedException {
        // Настраиваем capabilities.
        DesiredCapabilities capabilities = new DesiredCapabilities();

        capabilities.setCapability("deviceName", "Pixel");
        capabilities.setCapability("udid", "emulator-5554");
        capabilities.setCapability("platformName", "Android");
        capabilities.setCapability("platformVersion", "11");
        capabilities.setCapability("app", "/Users/Dver/Downloads/Android-NativeDemoApp-0.2.1.apk");

        // Устанавливаем и открываем приложение.
        MobileDriver driver = new AndroidDriver(new URL("http://localhost:4723/wd/hub"), capabilities);
        Thread.sleep(3000);

        // Тапаем на Login в основном меню.
        MobileElement menuLoginButton = (MobileElement) driver.findElementByAccessibilityId("Login");
        menuLoginButton.click();
        Thread.sleep(3000);

        // Заполняем поле email
        MobileElement emailInput = (MobileElement) driver.findElementByAccessibilityId("input-email");
        emailInput.click();
        emailInput.sendKeys(EMAIL);
        Thread.sleep(2000);

        // Тапаем на кнопку "Логин"
        MobileElement loginButton = (MobileElement) driver.findElementByXPath("//android.view.ViewGroup[@content-desc=\"button-LOGIN\"]/android.view.ViewGroup/android.widget.TextView");
        loginButton.click();
        Thread.sleep(3000);

        // Проверяем наличие сообщения о необходимости заполнить поле "Пароль"
        MobileElement errorPasswordText = (MobileElement) driver.findElementByXPath("//android.widget.ScrollView[@content-desc=\"Login-screen\"]/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup[4]/android.widget.TextView");
        Assert.assertEquals(errorPasswordText.getText(), EXPECTED);

        driver.quit();
    }
}
